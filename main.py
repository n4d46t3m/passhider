#!/usr/bin/python3

########################################################################
#
# Copyright (c) 2021-2022 n4d46t3m
#
#          _  _       _ _  _       
#    _ __ | || |   __| | || |  ___ 
#   | '_ \| || |_ / _` | || |_/ __|
#   | | | |__   _| (_| |__   _\__ \
#   |_| |_|  |_|  \__,_|  |_| |___/                               
#  
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
########################################################################

try:
    #import os # Maybe reimplement this and not "from os import something"
    from os import path as osPath
    from os import system as osSystem
    from os import remove as osRemove
    from os import listdir as osListdir
    from os import mkdir as osMkdir
    from sys import exit as sysExit
    from datetime import datetime
    import configparser
    import json
    import pyperclip
    from sty import fg, bg, ef, rs
    import encryptDecrypt
    import readline
    #import tarfile # Maybe reimplement this and not "from tarfile import open as tarfileOpen"
    from tarfile import open as tarfileOpen
    from configparser import ConfigParser
except Exception as err:
    print('An error occured while importing neccesary modules or library:')
    print(err)
    exit()

########################################################################
# Python deps:
#
#   PyperClip
#       pip3 install pyperclip
#           On Linux you should also install SW like xclip or xsel as deps
#           EG: sudo apt install xclip
#
#   STY
#       pip3 install sty
#
########################################################################

class PassHider:
    
    def __init__(self):
        '''
        HISTORY:
            In 1.0.10 Simple installation with PyInstaller and minor changes
        
            In 1.0.9 was introduced an ini configuration file
        
            In 1.0.8 users can generate new secret key and re encrypt 
            data file and backups
        
            In 1.0.7 implemented encryption/decryption and basic DEBUG system
        
            In 1.0.6 were created the method for restore without encryption
            
            In 1.0.5 were created the methods for modify and delete data 
            without encryption
        
            In 1.0.4 were created the methods for insert data and backup 
            data without encryption
            
            From 1.0.2 to 1.0.3 was added the first option menù with methods
            userInputOpt() and printSwOpt()
            
        '''
        self.swName='Pass Hider'#Software name
        self.swVers='1.0.10'#Software version
        self.workingDir=osPath.expanduser('~')+'/.passHider/' # Or './' for DEV or other purposes
        #self.workingDir=os.path.expanduser('~')+'/.passHider/' # Or './' for DEV or other purposes
        self.clearTerm()
        self.printGreeting()
        self.assetsDir='assets/'
        self.configFileName='conf.ini'
        if not self._getConfig(self.workingDir+self.assetsDir+self.configFileName):
            if not self._getConfig(self.assetsDir+self.configFileName):
                self.printExitGeneric()
                sysExit(1)
        self._generateSkeletonUserDir()
        self.data=[]#List that will contain the Json data taken from input file
        self.userChoice=None
        self.userChoiceOpt=None
        self.encryptDecrypt=encryptDecrypt.EncryptDecrypt('',self.workingDir+self.keyFile,self.workingDir+self.encryptedFile,False,False)#The first param is almost useless. In this case maybe useful only for testing
        self.isFirstRun=self._checkFirstRun()#Simply check if there is an encription key
        self.encryptionKey=self.encryptDecrypt.load_key()#Get encryption key
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Secret Key is: '+self.encryptionKey.decode()+rs.all)#DEBUG
        self.encryptDecrypt.initFernet(self.encryptionKey)#Init enc/dec system with loaded encryption key

    def _generateSkeletonUserDir(self):
        '''
        generateSkeletonUserDir 1.0
            Generate system working directory creating dirs and moving 
            files that are important for the software
        
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _generateSkeletonUserDir()"+rs.all)#DEBUG
        if not osPath.isdir(self.workingDir):
            print(ef.bold+fg.yellow+'Working directory not found, creating '+self.workingDir+rs.all)
            osMkdir(self.workingDir)
            open(self.workingDir+self.encryptedFile,'w').close()
        else:
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Working directory '+self.workingDir+' found'+rs.all) # DEBUG
        if not osPath.isdir(self.backupDir):
            print(ef.bold+fg.yellow+'Backup directory not found, creating '+self.backupDir+rs.all)
            osMkdir(self.backupDir)
        else:
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Backup directory '+self.backupDir+' found'+rs.all) # DEBUG
        if not osPath.isdir(self.oldEncryptionDir):
            print(ef.bold+fg.yellow+'Old encryptions directory not found, creating '+self.oldEncryptionDir+rs.all)
            osMkdir(self.oldEncryptionDir)
        else:
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Old encryptions directory '+self.oldEncryptionDir+' found'+rs.all) # DEBUG
        if not osPath.isdir(self.workingDir+self.assetsDir):
            print(ef.bold+fg.yellow+'Assets directory not found, creating '+self.workingDir+self.assetsDir+rs.all)
            osMkdir(self.workingDir+self.assetsDir)
            osSystem('cp '+self.assetsDir+self.configFileName+' '+self.workingDir+self.assetsDir+self.configFileName) 
        else:
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Assets directory '+self.workingDir+self.assetsDir+' found'+rs.all) # DEBUG
        osSystem('chmod -R 700 '+self.workingDir)
        

    def _getConfig(self,configFile=''):
        '''
        _getConfig 1.1
            Read an ini file and get some configurations
        
        :param configFile: Full path and filename of the configuration file
        
        :return: True in case of success, otherwise false
        '''
        conf = ConfigParser()
        try:
            print(ef.bold+fg.yellow+'INIT: Loading configuration file '+configFile+rs.all)# DEBUG
            with open(configFile) as configFile:
                conf.read_file(configFile)
        except:
            print(ef.bold+fg.red+'INIT: Unable to read configuration file at '+configFile+rs.all)
            return False
        try:
            self.debug = conf.getboolean('output','cliDebugMode')
            self.verbose = conf.getboolean('output','cliVerboseMode')#Not yet implemented (Maybe useless at this time)
            if self.debug:
                print()#DEBUG
                print(ef.bold+fg.yellow+'DEBUG MODE ON !!! Maybe some users will be able to see you secrets in clear'+rs.all)#DEBUG
                print()#DEBUG
                print(ef.bold+fg.yellow+'DEBUG: self.debug = '+str(self.debug)+rs.all)#DEBUG
                print(ef.bold+fg.yellow+'DEBUG: self.verbose = '+str(self.verbose)+rs.all)#DEBUG
        except ValueError:
            print(ef.bold+fg.red+'conf.ini: cliDebugMode and cliVerboseMode must be equal to 0 or 1'+rs.all)
            return False
        except Exception as e:
            print(ef.bold+fg.red+str(e)+rs.all)#DEBUG
            print(ef.bold+fg.red+'conf.ini: error getting cliDebugMode and cliVerboseMode'+rs.all)
            return False
        try:
            self.keyFile = conf.get('input','keyFile')
            self.encryptedFile = conf.get('input','dataFile')#The input file that contain Json encrypted data
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: self.keyFile = '+str(self.workingDir+self.keyFile)+rs.all)#DEBUG
                print(ef.bold+fg.yellow+'DEBUG: self.encryptedFile = '+str(self.workingDir+self.encryptedFile)+rs.all)#DEBUG
        except Exception as e:
            print(ef.bold+fg.red+str(e)+rs.all)#DEBUG
            print(ef.bold+fg.red+'conf.ini: error getting keyFile and/or dataFile'+rs.all)
            return False
        try:
            self.backupDir = self.workingDir+conf.get('input','backupDir')#The path of backup files
            self.oldEncryptionDir = self.workingDir+conf.get('input','oldEncryptionDir')#The path where are stored old encrypted files and secret key when generating a new key
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: self.backupDir = '+str(self.backupDir)+rs.all)#DEBUG
                print(ef.bold+fg.yellow+'DEBUG: self.oldEncryptionDir = '+str(self.oldEncryptionDir)+rs.all)#DEBUG
        except Exception as e:
            print(ef.bold+fg.red+str(e)+rs.all)#DEBUG
            print(ef.bold+fg.red+'conf.ini: error getting backupDir and/or oldEncryptionDir'+rs.all)
            return False
        return True

    def _checkFirstRun(self):
        '''
        _checkFirstRun 1.0
            If the encryption key is not found it will be generated.
            It is assumed that if encryption key isn't found is the 
                first time that this software is run.
        
        :return: True if is the first time that the software is run, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _checkFirstRun()"+rs.all)#DEBUG
        if osPath.isfile(self.encryptDecrypt.getKeyFileName()):
        #if os.path.isfile(self.encryptDecrypt.getKeyFileName()):
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Found an encryption key, try to load it'+rs.all)#DEBUG
            return True
        else:
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Encryption key not found, now I will generate it'+rs.all)#DEBUG
            self.encryptDecrypt.write_key()
            return False

    def printGreeting(self):
        '''
        printGreeting 1.0
            Simply print the starting message with things like software 
            name and version
        
        :return: void
        '''
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Welcome to '+self.swName+' v'+self.swVers+' '+rs.all)
        print()
            
        
    def selectFile(self):
        '''
        selectFile 1.0
            Let the user to choice the file to parse through a prompt.
            The user must provide valid data file (maybe with path)
        
        TODO: Check the given file and the call self.jsonFileToPy()
        
        :return: void
        '''
        self.encryptedFile = str(input(ef.bold+fg.li_green+bg.da_magenta+'Enter the file that you want to parse : '+rs.all))
        self.jsonFileToPy()

    def jsonFileToPy(self):
        '''
        jsonFileToPy 1.1
            Reverse the file to a list
        
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in jsonFileToPy()"+rs.all)#DEBUG
        try:
            #print(self.encryptedFile)#DEBUG
            self.encryptedData=self.encryptDecrypt.loadTextFile(self.workingDir+self.encryptedFile)
            #print(self.encryptedData)#DEBUG
            if len(self.encryptedData) == 0:
                self.encryptedData = self.encryptDecrypt.encryptString('[]'.encode())
                self.encryptDecrypt.writeEncryptedStringToFile(self.encryptedData)
            self.decryptedData=self.encryptDecrypt.decryptString(self.encryptedData)
            #print(self.decryptedData)#DEBUG
            self.data = json.loads(self.decryptedData.decode('utf-8'))
            #print(self.data)#DEBUG
            return True
        except FileNotFoundError:
            print(ef.bold+fg.red+'File not found'+rs.all)
            self.selectFile()
            return False
        #except Exception as err:
        #    if self.debug:
        #        print(ef.bold+fg.red+'DEBUG: Error in jsonFileToPy()'+rs.all)#DEBUG
        #    self.printGenericError(err)
        #    return False
            
    def pyToJsonFile(self):
        '''
        pyToJsonFile 1.0
            Take current loaded list of dictionaries and create a Json file
            
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in pyToJsonFile()"+rs.all)#DEBUG
        try:
            encryptedData = self.encryptDecrypt.encryptString(json.dumps(self.data).encode())
            self.encryptDecrypt.writeEncryptedStringToFile(encryptedData)
            return True
        except Exception as err:
            if self.debug:
                print(ef.bold+fg.red+'DEBUG: Error in pyToJsonFile()'+rs.all)#DEBUG
            self.printGenericError(err)
            return False
            
    def _execModify(self,itemNumber=None):
        '''
        _execModify 1.0
            Modify given item and save it to data file
            
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _execModify()"+rs.all)#DEBUG
        if not self._backupFile():
            print(ef.bold+fg.red+'Something was wrong during data file backup'+rs.all)
            return False
        oldItemData=self.data[itemNumber]
        readline.set_startup_hook(lambda: readline.insert_text(self.data[itemNumber]['Desc']))
        try:
            self.data[itemNumber]['Desc'] = str(input('Enter description: '))
        finally:
            readline.set_startup_hook()
        readline.set_startup_hook(lambda: readline.insert_text(self.data[itemNumber]['User']))
        try:
            self.data[itemNumber]['User'] = str(input('Enter username: '))
        finally:
            readline.set_startup_hook()
        readline.set_startup_hook(lambda: readline.insert_text(self.data[itemNumber]['Pass']))
        try:
            self.data[itemNumber]['Pass'] = str(input('Enter password: '))
        finally:
            readline.set_startup_hook()
        readline.set_startup_hook(lambda: readline.insert_text(self.data[itemNumber]['Email']))
        try:
            self.data[itemNumber]['Email'] = str(input('Enter associated mail: '))
        finally:
            readline.set_startup_hook()
        readline.set_startup_hook(lambda: readline.insert_text(self.data[itemNumber]['TFA']))
        try:
            self.data[itemNumber]['TFA'] = str(input('Enter two factor auth: '))
        finally:
            readline.set_startup_hook()
        readline.set_startup_hook(lambda: readline.insert_text(self.data[itemNumber]['Info']))
        try:
            self.data[itemNumber]['Info'] = str(input('Enter infos: '))
        finally:
            readline.set_startup_hook()
        #print(self.data)#DEBUG
        if self.pyToJsonFile():
            self.jsonFileToPy()
            return True
        else:
            print(ef.bold+fg.red+'Something was wrong writing data file'+rs.all)
            self.data[itemNumber] = oldItemData
            return False
    
    def _execDelete(self,itemNumber=None):
        '''
        _execDelete 1.0
            Delete given item and save it to data file
            
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _execDelete()"+rs.all)#DEBUG
        if not self._backupFile():
            print(ef.bold+fg.red+'Something was wrong during data file backup'+rs.all)
            return False
        itemData=self.data[itemNumber]
        self.data.pop(itemNumber)
        if self.pyToJsonFile():
            self.jsonFileToPy()
            return True
        else:
            print(ef.bold+fg.red+'Something was wrong writing data file'+rs.all)
            self.data.insert(itemNumber,itemData)
            return False
            
    def _backupFile(self):
        '''
        _backupFile 1.1
            Backup JSON file creating backups like filename.YYYYmmddHHMMSS.bck
            
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _backupFile()"+rs.all)#DEBUG
        try:
            # Backup current item file, I use os because is already imported, try shutil for more crossplatform
            if osSystem('cp '+self.workingDir+self.encryptedFile+' '+self.backupDir+self.encryptedFile+'.'+datetime.now().strftime("%Y%m%d%H%M%S")+'.bck') != 0:
            #if os.system('cp '+self.workingDir+self.encryptedFile+' '+self.backupDir+self.encryptedFile+'.'+datetime.now().strftime("%Y%m%d%H%M%S")+'.bck') != 0:
                print(ef.bold+fg.red+"I can't create backup file"+rs.all)#DEBUG
                return False
        except Exception as err:
            self.printGenericError(err)
            return False
        return True
        
    def _restoreFile(self,backupFile=''):
        '''
        _restoreFile 1.0
            Restore given JSON file
            
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _restoreFile()"+rs.all)#DEBUG
        try:
            # Restore given item file, I use os because is already imported, try shutil for more crossplatform
            if osSystem('cp '+backupFile+' '+self.workingDir+self.encryptedFile) != 0:
            #if os.system('cp '+backupFile+' '+self.workingDir+self.encryptedFile) != 0:
                print(ef.bold+fg.red+"I can't restore given file"+rs.all)#DEBUG
                return False
            else:
                self.jsonFileToPy()
        except Exception as err:
            self.printGenericError(err)
            return False
        return True
        
    def _generateSecretKey(self):
        '''
        _generateSecretKey 1.0
            Backup old data file, secret key and backupped data files.
            Generate a new secret key.
            Reencrypt data file and backupped data files with the new key
        
        :return: True in case of success, otherwise false
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in _generateSecretKey()"+rs.all)#DEBUG
        #compressedFileName='asd.tar.bz2'#TESTING
        compressedFileName=datetime.now().strftime("%Y%m%d%H%M%S")+'.tar.bz2'
        compressedFile=self.oldEncryptionDir+compressedFileName
        try:
            tarObj = tarfileOpen(mode='x:bz2', fileobj=compressedFile)
            #tarObj = tarfile.open(mode='x:bz2', fileobj=compressedFile)
        except FileExistsError:
            print(ef.bold+fg.red+'File '+compressedFile+' already exists'+rs.all)
            return False
        except Exception as e:
            print(ef.bold+fg.red+str(e)+rs.all)
            print(ef.bold+fg.red+'Something went wrong when creating '+compressedFile+rs.all)
            return False
        try:
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Created '+str(tarObj)+rs.all)#DEBUG
                print(ef.bold+fg.yellow+'DEBUG: Adding '+self.encryptedFile+' to '+compressedFile+rs.all)
            tarObj.add(self.workingDir+self.encryptedFile)
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Adding '+self.encryptDecrypt.getKeyFileName()+' to '+compressedFile+rs.all)
            tarObj.add(self.encryptDecrypt.getKeyFileName())
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Adding '+self.backupDir+' to '+compressedFile+rs.all)
            tarObj.add(self.backupDir)
        except Exception as e:
            print(ef.bold+fg.red+str(e)+rs.all)
            print(ef.bold+fg.red+'Something went wrong adding files to '+compressedFile+rs.all)
            tarObj.close()
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Removing '+compressedFile+rs.all)
            osRemove(compressedFile)
            #os.remove(compressedFile)
            return False
        tarObj.close()
        # Creare la nuova key key.key.new
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Creating '+self.encryptDecrypt.getKeyFileName()+'.new'+rs.all)
        self.encryptDecrypt.write_key('wb',self.encryptDecrypt.getKeyFileName()+'.new')
        # Copiare la vecchia key in key.key.old
        #if self.debug:
        #    print(ef.bold+fg.yellow+'DEBUG: Copy key.key in key.key.old'+rs.all)
        #if osSystem('cp '+self.encryptDecrypt.getKeyFileName()+' '+self.encryptDecrypt.getKeyFileName()+'.old') != 0:
        #if os.system('cp '+self.encryptDecrypt.getKeyFileName()+' '+self.encryptDecrypt.getKeyFileName()+'.old') != 0:
        #    print(ef.bold+fg.red+"I can't copy key.key to key.key.old"+rs.all)#DEBUG
        #    if self.debug:
        #        print(ef.bold+fg.yellow+'DEBUG: Removing key.key.new')
        #    osRemove('key.key.new')
        #    os.remove('key.key.new')
        #    return False
        # Re-encrypt data file
        self._reEncryptFile(self.workingDir+self.encryptedFile)
        # Re-encrypt dei file presenti nella cartella backup
        backupFiles = sorted([f for f in osListdir(self.backupDir)])
        #backupFiles = sorted([f for f in os.listdir(self.backupDir)])
        for idx,bckFileName in enumerate(backupFiles):
            if self.debug:
                print(ef.bold+fg.yellow+'DEBUG: Try to re encrypt '+self.backupDir+bckFileName+rs.all)
            self._reEncryptFile(self.backupDir+bckFileName)
        # Reload the encrypted JSON
        osRemove(self.encryptDecrypt.getKeyFileName())
        #os.remove(self.encryptDecrypt.getKeyFileName())
        osSystem('mv '+self.encryptDecrypt.getKeyFileName()+'.new '+self.encryptDecrypt.getKeyFileName())
        #os.system('mv '+self.encryptDecrypt.getKeyFileName()+'.new '+self.encryptDecrypt.getKeyFileName())
        self.jsonFileToPy()
        return True
          
    def _reEncryptFile(self, fileToReEncrypt=''):
        '''
        _reEncryptFile 1.0
            Re-Encrypt given file (this will rewrite given file)
            
        :param fileToReEncrypt: the file that should be re encrypted
        
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Loading old encrypted data'+rs.all)#DEBUG
        oldEncryptedData = self.encryptDecrypt.loadTextFile(fileToReEncrypt)
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: '+oldEncryptedData.decode()+rs.all)#DEBUG
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Decrypting old encrypted data'+rs.all)#DEBUG
        oldKeyLaded = self.encryptDecrypt.load_key('rb',self.encryptDecrypt.getKeyFileName())
        self.encryptDecrypt.initFernet(oldKeyLaded)
        oldDecryptedData = self.encryptDecrypt.decryptString(oldEncryptedData)
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: '+oldDecryptedData.decode()+rs.all)#DEBUG
            print(ef.bold+fg.yellow+'DEBUG: Loading '+self.encryptDecrypt.getKeyFileName()+'.new'+rs.all)#DEBUG
        newKeyLaded = self.encryptDecrypt.load_key('rb',self.encryptDecrypt.getKeyFileName()+'.new')
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Init Fernet with the new key'+rs.all)#DEBUG
        self.encryptDecrypt.initFernet(newKeyLaded)
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Re-Encrypting data file'+rs.all)#DEBUG
        newEncryptedData = self.encryptDecrypt.encryptString(oldDecryptedData)
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Writing Re-Encrypted data to '+fileToReEncrypt+rs.all)#DEBUG
        self.encryptDecrypt.writeEncryptedStringToFile(newEncryptedData,'wb',fileToReEncrypt)
            
    def printAvailableItems(self):
        '''
        printAvailableItems 1.1
            Print the list of the items found in input file. 
            And some software options
        
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in printAvailableItems()"+rs.all)#DEBUG
        print()
        print("\t"+ef.bold+'[666] '+ef.rs+'PROGRAM OPTIONS')
        print("\t"+ef.bold+'[999] '+ef.rs+'EXIT')
        print()
        for idx,item in enumerate(self.data):
            print("\t"+ef.bold+'['+str(idx)+'] '+ef.rs+item['Desc'])
        print()
    
    def printExitMessage(self):
        '''
        printExitMessage 1.0
            This should be used when all is done in the right way
        
        :return: void
        '''
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+'The password of selected item now is in your clipboard, see you later !!!'+rs.all)
        print()
        
    def printExitGeneric(self):
        '''
        printExitGeneric 1.0
            An exit without any information
        
        :return: void
        '''
        print()
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' See you soon '+rs.all)
        print()

    def userInput(self):
        '''
        userInput 1.0
            User can select the wanted item, this will modify 
            self.userChoice
        
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in userInput()"+rs.all)#DEBUG
        try:
            self.userChoice = int(input(ef.bold+fg.li_green+bg.da_magenta+'Enter the number corrisponding to the item that you want show: '+rs.all))
            return True
        except ValueError:
            print(ef.bold+fg.red+'You must enter a valid integer'+rs.all)
            self.userInput()
            return False
        except Exception as err:
            self.printGenericError(err)
            self.userInput()
            return False
            
    def userInputOpt(self):
        '''
        userInputOpt 1.0
            User can select the wanted option, this will modify 
            self.userChoiceOpt
        
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in userInputOpt()"+rs.all)#DEBUG
        try:
            self.userChoiceOpt = int(input(ef.bold+fg.li_green+bg.da_magenta+'Enter the number corrisponding the action that you want perform: '+rs.all))
            return True
        except ValueError:
            print(ef.bold+fg.red+'You must enter a valid integer'+rs.all)
            self.userInputOpt()
            return False
        except Exception as err:
            self.printGenericError(err)
            self.userInputOpt()
            return False
            
    def printSwOpt(self):
        '''
        printSwOpt 1.0
            Print the option menù
        
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in printSwOpt()"+rs.all)#DEBUG
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Now you are in options area !!! '+rs.all)
        print()
        print("\t"+ef.bold+'[1] '+ef.rs+'Insert')
        print("\t"+ef.bold+'[2] '+ef.rs+'Modify')
        print("\t"+ef.bold+'[3] '+ef.rs+'Delete')
        print("\t"+ef.bold+'[4] '+ef.rs+'Backup data')
        print("\t"+ef.bold+'[5] '+ef.rs+'Restore data')
        print("\t"+ef.bold+'[6] '+ef.rs+'Generate new secret key')
        print("\t"+ef.bold+'[999] '+ef.rs+'BACK')
        print()
        self.userInputOpt()
        if self.userChoiceOpt == 999:
            self.printAvailableItems()
            print()
            self.userInput()
            self.printInfos()
        elif self.userChoiceOpt == 1:
            self.addItem()
        elif self.userChoiceOpt == 2:
            self.modifyItem()
        elif self.userChoiceOpt == 3:
            self.deleteItem()
        elif self.userChoiceOpt == 4:
            self.backupFileCliInterface()
        elif self.userChoiceOpt == 5:
            self.restoreFileCliInterface()
        elif self.userChoiceOpt == 6:
            self.generateSecretKeyCliInterface()
        else:
            print(ef.bold+fg.red+'You must enter a valid option value !!!'+rs.all)
            self.printSwOpt()

    def generateSecretKeyCliInterface(self):
        '''
        generateSecretKeyCliInterface 1.0
            Command line interface to generate a new secret key used for 
            encrypt/decrypt data file and backups
            
        :return: void
        '''
        print(ef.bold+fg.yellow+'NOTE that if you generate a new key, data file and backups will be re-encrypt'+rs.all)
        newKeyConfirm = input(ef.bold+fg.li_green+bg.da_magenta+'Are you sure to create a new secret key? [Y,n] '+rs.all)
        if newKeyConfirm == 'Y' or newKeyConfirm == 'y':
            self._generateSecretKey()
        elif newKeyConfirm == 'n':
            print(ef.bold+fg.li_green+bg.da_magenta+'Exiting without creating the new key'+rs.all)
            self.printSwOpt()
        else:
            print(ef.bold+fg.red+'Wrong choice'+rs.all)
            self.generateSecretKeyCliInterface()
        self.printSwOpt()

    def modifyItem(self):
        '''
        modifyItem 1.1
            Command line interface to modify an item of credentials list
            
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in modifyItem()"+rs.all)#DEBUG
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Modify an item '+rs.all)
        print()
        self.printAvailableItems()
        try:
            modifyChoice = int(input(ef.bold+fg.li_green+bg.da_magenta+'Enter the number of the item that you want modify: '+rs.all))
        except ValueError as err:
            print(ef.bold+fg.red+'Only integers are admitted'+rs.all)
            self.modifyItem()
        if modifyChoice == 666:
            self.printSwOpt()
        elif modifyChoice == 999:
            self.printExitGeneric()
            sysExit(0)
        elif modifyChoice >= 0 and modifyChoice <= len(self.data)-1:
            if not self._execModify(modifyChoice):
                print(ef.bold+fg.red+'Something was wrong modifing data'+rs.all)
            self.printSwOpt()
        else:
            if len(self.data) > 0:
                print(ef.bold+fg.red+'You should select an item from 0 to '+str(len(self.data)-1)+rs.all)
            else:
                print(ef.bold+fg.red+'Nothing to modify, you should create the first item'+rs.all)
            self.modifyItem()

    def deleteItem(self):
        '''
        deleteItem 1.1
            Command line interfsce for delete an item from credentials list
            
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in deleteItem()"+rs.all)#DEBUG
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Delete an item '+rs.all)
        print()
        self.printAvailableItems()
        try:
            deleteChoice = int(input(ef.bold+fg.li_green+bg.da_magenta+'Enter the number of the item that you want delete: '+rs.all))
        except ValueError as err:
            print(ef.bold+fg.red+'Only integers are admitted'+rs.all)
            self.deleteItem()
        if deleteChoice == 666:
            self.printSwOpt()
        elif deleteChoice == 999:
            self.printExitGeneric()
            sysExit(0)
        elif deleteChoice >= 0 and deleteChoice <= len(self.data)-1:
            if not self._execDelete(deleteChoice):
                print(ef.bold+fg.red+'Something was wrong deleting data'+rs.all)
            self.printSwOpt()
        else:
            if len(self.data) > 0:
                print(ef.bold+fg.red+'You should select an item from 0 to '+str(len(self.data)-1)+rs.all)
            else:
                print(ef.bold+fg.red+'Nothing to delete, you should create the first item'+rs.all)
            self.deleteItem()
    
    def backupFileCliInterface(self):
        '''
        backupFileCliInterface 1.0
            Command line interface for backup credential file
            call _backupFile and print results in command line
        
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in backupFileCliInterface()"+rs.all)#DEBUG
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Executing backup of current used file '+rs.all)
        print()
        # Execute backup of current file 
        if not self._backupFile():
            print(ef.bold+fg.red+'Something was wrong during data file backup'+rs.all)
        else:
            print('Successfully created backup file')
        self.printSwOpt()
        
    def restoreFileCliInterface(self):
        '''
        restoreFileCliInterface 1.1
            Command line interface to restore credential data from a list of files
        
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in restoreFileCliInterface()"+rs.all)#DEBUG
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Restore data choosing wanted backup '+rs.all)
        print(ef.bold+fg.li_green+bg.da_magenta+' NOTE !!! After you have selected the file, it will displayed all contents in clear '+rs.all)
        print()
        backupFiles = sorted([f for f in osListdir(self.backupDir)])
        #backupFiles = sorted([f for f in os.listdir(self.backupDir)])
        if self.debug:
            print(ef.bold+fg.li_yellow+'DEBUG: Here is the list of found backup files:'+rs.all)#DEBUG
            print(ef.bold+fg.li_yellow+str(backupFiles)+rs.all,end="\n\n")#DEBUG
        print("\t"+ef.bold+'[666] '+ef.rs+'PROGRAM OPTIONS')
        print("\t"+ef.bold+'[999] '+ef.rs+'EXIT')
        print()
        for idx,bckFileName in enumerate(backupFiles):
            print("\t"+ef.bold+'['+str(idx)+'] '+ef.rs+bckFileName)
        print()
        self.userInput()
        if self.userChoice == 666:
            self.printSwOpt()
        elif self.userChoice == 999:
            print()
            sysExit(0)
        elif self.userChoice >=0 and self.userChoice<=len(backupFiles)-1:
            print('You have selected backup file '+self.backupDir+backupFiles[self.userChoice])
            print(ef.bold+'Here is the content of '+self.backupDir+backupFiles[self.userChoice]+rs.all)
            print()
            try:
                with open(self.backupDir+backupFiles[self.userChoice]) as json_enc_file:
                    jsonEncStr=json_enc_file.read()
                    #print(jsonEncStr)#DEBUG
                    decryptedBackup = self.encryptDecrypt.decryptString(jsonEncStr.encode())
                    #print(decryptedBackup)#DEBUG
                    backupDataToRestore = json.loads(decryptedBackup.decode('utf-8'))
                    #print(backupDataToRestore)#DEBUG
                    for itemData in backupDataToRestore:
                        print(ef.bold+"Description:\t\t"+ef.rs+itemData['Desc'])
                        print(ef.bold+"Username:\t\t"+ef.rs+itemData['User'])
                        print(ef.bold+"Password:\t\t"+ef.rs+itemData['Pass'])
                        print(ef.bold+"Associated Email:\t"+ef.rs+itemData['Email'])
                        print(ef.bold+"Second Auth:\t\t"+ef.rs+itemData['TFA'])
                        print(ef.bold+"Related Notes:\t\t"+ef.rs+itemData['Info'])
                        print()
                    restoreConfirm = input(ef.bold+fg.li_green+bg.da_magenta+'Are you sure to restore '+self.backupDir+backupFiles[self.userChoice]+'? [Y,n] '+rs.all)
                    if restoreConfirm == 'Y' or restoreConfirm == 'y':
                        print(ef.bold+'Backupping current datafile ... '+rs.all)
                        if not self._backupFile():
                            print(ef.bold+fg.red+'Something was wrong during data file backup'+rs.all)
                            self.restoreFileCliInterface()
                        else:
                            print(ef.bold+'Restoring '+self.backupDir+backupFiles[self.userChoice]+' ... '+rs.all)
                            if not self._restoreFile(self.backupDir+backupFiles[self.userChoice]):
                                print(ef.bold+fg.red+'Something was wrong during the restore'+rs.all)
                                self.restoreFileCliInterface()
                            else:
                                print(ef.bold+'Restore completed successfully ... '+rs.all)
                                self.printSwOpt()
                    elif restoreConfirm == 'n':
                        print(ef.bold+'Anything to restore...'+rs.all)
                        self.restoreFileCliInterface()
                    else:
                        print(ef.bold+fg.red+'Wrong option, you should retry...'+rs.all)
                        self.restoreFileCliInterface()
            except FileNotFoundError:
                print(ef.bold+fg.red+'File not found'+rs.all)
                self.restoreFileCliInterface()
            except Exception as err:
                self.printGenericError(err)
        else:
            if len(backupFiles) > 0:
                print(ef.bold+fg.red+'You should select an item from 0 to '+str(len(backupFiles)-1)+rs.all)
            else:
                print(ef.bold+fg.red+'Nothing to restore, you should first perform a backup'+rs.all)
            self.restoreFileCliInterface()
        self.printSwOpt()
    
    def addItem(self):
        '''
        addItem 1.0
            Add an item to credentials list from command line
            
        :return: void
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in addItem()"+rs.all)#DEBUG
        print()
        print(ef.bold+fg.li_green+bg.da_magenta+' Add an item to credentials list '+rs.all)
        print()
        newItem = {}
        newDesc = str(input('Enter description: '))
        newItem['Desc'] = newDesc
        newUser = str(input('Enter username: '))
        newItem['User'] = newUser
        newPass = str(input('Enter password: '))
        newItem['Pass'] = newPass
        newMail = str(input('Enter associated mail: '))
        newItem['Email'] = newMail
        newTFA = str(input('Enter two factor auth: '))
        newItem['TFA'] = newTFA
        newInfo = str(input('Enter infos: '))
        newItem['Info'] = newInfo
        #print(self.data)#DEBUG
        #print(newItem)#DEBUG
        self.data.append(newItem)
        #print(self.data)#DEBUG
        # Execute backup of current file 
        if self.debug:
            print(ef.bold+fg.yellow+'DEBUG: Executing backup'+rs.all)
        if not self._backupFile():
            print(ef.bold+fg.red+'Something was wrong during data file backup'+rs.all)
            self.data.pop()
            self.printSwOpt()
        if self.pyToJsonFile():
            self.jsonFileToPy()
        else:
            print(ef.bold+fg.red+'Something was wrong writing data file'+rs.all)
            self.data.pop()
            self.printSwOpt()
        insertAnother = str(input('Do you want insert another item? [Y/n] '))
        if insertAnother == 'Y' or insertAnother == 'y':
            self.addItem()
        else:
            self.printSwOpt()
    
    def printInfos(self):
        '''
        printInfos 1.2
            Print on stdin some informations of selected item and copy 
            to clipboard the password but some magic numbers can redirect
            the user to some features such as software options or commands
        
        :return: True in case of success, otherwise False
        '''
        if self.debug:
            print(ef.bold+fg.yellow+"DEBUG: I'm in printInfos()"+rs.all)#DEBUG
        print()
        if self.userChoice == 666:
            self.printSwOpt()
            return True
        if self.userChoice == 999:
            sysExit(0)
        try:
            print(ef.bold+"Description:\t\t"+ef.rs+ self.data[self.userChoice]['Desc'])
            print(ef.bold+"Username:\t\t"+ef.rs+ self.data[self.userChoice]['User'])
            if self.data[self.userChoice]['TFA'] != '':
                print(ef.bold+"Second Auth:\t\t"+ef.rs+ self.data[self.userChoice]['TFA'])
            if self.data[self.userChoice]['Email'] != '':
                print(ef.bold+"Associated Email:\t"+ef.rs+ self.data[self.userChoice]['Email'])
            if self.data[self.userChoice]['Info'] != '':
                print(ef.bold+"Related Notes:\t\t"+ef.rs+ self.data[self.userChoice]['Info'])
            pyperclip.copy(self.data[self.userChoice]['Pass'])
            self.printExitMessage()
            #self.anyKeyToExit()#Maybe is useful let the user to choice to use self.anyKeyToExit() or self.exitOrNot() throug a config in conf.ini
            self.exitOrNot()#Maybe is useful let the user to choice to use self.anyKeyToExit() or self.exitOrNot() throug a config in conf.ini
            return True
        except IndexError:
            if len(self.data) > 0:
                print(ef.bold+fg.red+'You should insert a number from 0 to '+str(len(self.data)-1)+rs.all)
            else:
                print(ef.bold+fg.red+'Nothing to select, you should insert the first item'+rs.all)
            #self.userChoice = self.userInput()
            self.userInput()
            #self.printInfos(self.userChoice)
            self.printInfos()
            return False
        except Exception as err:
            self.printGenericError(err)
            #self.userChoice = self.userInput()
            self.userInput()
            #self.printInfos(self.userChoice)
            self.printInfos()
            return False
            
    def anyKeyToExit(self):
        '''
        anyKeyToExit 1.0
            After user input, exit program
        
        :return: void
        '''
        print()
        str(input(ef.bold+fg.li_green+bg.da_magenta+'Press any key to exit, the password may disappear from clipboard if you close this software '+rs.all))
        print()
        sysExit(0)
        
    def exitOrNot(self):
        '''
        exitOrNot 1.0
            Y to mantain software opened, any key to quit
        
        :return: void
        '''
        print()
        input(ef.bold+fg.li_green+bg.da_magenta+'The password may disappear from clipboard if you close this software '+rs.all)
        exitProgram = str(input(ef.bold+fg.li_green+bg.da_magenta+'Press q to quit or any key to continue to use this software '+rs.all))
        print()
        if exitProgram == 'q':
            sysExit(0)
        else:
            self.startSw()
            
    def printGenericError(self, error):
        '''
        printGenericError 1.0
        
        :param error: this is an Exception obj
        :return: void
        '''
        print(ef.bold+fg.red+'Generic Error'+rs.all)
        print(ef.bold+fg.red+str(error)+rs.all)
        
    def clearTerm(self):
        '''
        clearTerm 1.0
            Clear the Terminal
        
        :return: void
        '''
        osSystem('clear')
        #os.system('clear')
        
    def startSw(self):
        '''
        startSw 1.0
            Call some methods to start the software
        
        :return: void
        '''
        self.jsonFileToPy()#Maybe this is better in another place...
        self.printAvailableItems()
        self.userInput()
        self.printInfos()
        
########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    try:
        passHider = PassHider()
        passHider.startSw()
    except KeyboardInterrupt:
        passHider.printExitGeneric()
    #except Exception as err:## When coding, is useful comment this Exception
    #    passHider.printGenericError(err)
