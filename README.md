# Pass Hider 1.0.10
Small command line utility that can be used to copy passwords to clipboard to avoid to digit them or copy from some readable file<br>
Also useful in video meetings when you share the screen to avoid copy/paste password from text files (this is also applicable with shoulder surfers).<br>
If you want more secure and integrated OS software look for something else...<br><br>
_Coded and tested only on **Linux**_<br><br>
##
### Distributions used to test this software:
- [Debian](https://www.debian.org/distrib/ "Download Debian")
- [Ubuntu](https://ubuntu.com/download/ "Download Ubuntu")
- [Fedora](https://getfedora.org/ "Download Fedora")
- [Arch](https://archlinux.org/download/ "Download Arch Linux")
- Maybe all distros on that you can install **Requirements**
### Requirements:
- Python 3.x
- You should install software like `xclip` or `xsel` as **pyperclip** 
deps, here some install command examples:
	- Debian, Ubuntu and other distros that use `apt`: `sudo apt update && sudo apt install xclip`
	- Fedora and other distros that use `yum`: `sudo yum install xclip`
	- Arch and other distros that use `pacman`: `sudo pacman -Syu xclip`
- Need **sty** Python library `pip3 install sty` 
- Need **pyperclip** Python library `pip3 install pyperclip` 
### Configurations
- Almost all configs are located in `assets/conf.ini`<br>
- Maybe you want modify it before running the software the first time<br>
- When you run the software the first time, the configuration file will copied in `~/.passHider/assets/conf.ini`<br>
- If you have run the software at least one time, if you want change some configs you should modify `~/.passHider/assets/conf.ini` and not the `conf.ini` located in the `assets` directory of the project.
- Note that if you modify the `[input]` section, files and dirs were always created under `~/.passHider/` that is the harcoded working dir unless you modify var `self.workingDir` in `main.py`
### How to use
- Run in terminal `python3 main.py`<br>
- By default the software will create `.passHider` folder in your home directory<br>
- Assuming that you don't have modified `backupDir` and `oldEncryptionDir` in `conf.ini`<br>
  - In folder `backup` are stored varius version of the data file. Every time that the user perform an item insert, item modify, item delete, manual backup using `Backup data` or a restore using `Restore data`, the current data file will backupped in `backup` folder<br>
  - In folder `oldEncryption` are stored current data file, backups and the secret key when the user generate a new secret key selecting `Generate new secret key` from option menù<br>
  - To restore data from `oldEncryption` close the program if opened and copy ALL content of wanted compressed file in the working directory of the project that should br `~/.passHider`
### How to install using install.sh
- Go in **scripts** directory
- In `install.sh` change values of `outputBinPath` `outputDirPath` `applicationsPath` and `pyInstallerPath` as you want
- Make sure that the environment variable `HOME` is set before run `install.sh`
- Make sure that you have execution permission on `./install.sh` and then run it
- **NOTE** that the user that run the installation script must be able to perform `sudo` but the user shouldn't run it with `sudo` because the `sudo` command is already used when necessary in `./install.sh`. If you change values of `outputBinPath` `outputDirPath` and `applicationsPath` with paths where your user have write permissions `sudo` became useless and then you should remove all occurrences of `sudo` in `install.sh`
### TODO
- Implement a **GUI** with **tkinter** (this also mean that some code parts should be abstracted in a better way)
- In `install.sh` implement checks to better manage old installations
- In `install.sh` automate the use of `sudo`
- Let the user to change the working directory of the software that now is harcoded in `main.py` to `~/.passHider/` 
- Investigate why `KeyboardInterrupt` (`CTRL+C`) on compiled project isn't working wery well (**I think that this is caused by a PyInstaller bug**) **NOTE** that if you run the software in clear Python (EG: `python3 main.py`) `KeyboardInterrupt` works as expected
- Decrease executable filesize created by `install.sh`. I have already tried to use mirate imports, for example in `main.py` I use imports like `from os import path as osPath` in place of the generic `import os` but in this way the generated executable absurdly is bigger than using the simple `import os`

<br><br><br><br><br><br>
**CopyLeft n4d4s nc 2021-2022**
