#!/bin/bash

########################################################################
#
# n4d46t3m netcat 2021-2022 copyLeft or MIT
#
#          _  _       _ _  _       
#    _ __ | || |   __| | || |  ___ 
#   | '_ \| || |_ / _` | || |_/ __|
#   | | | |__   _| (_| |__   _\__ \
#   |_| |_|  |_|  \__,_|  |_| |___/                               
#                                     
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
########################################################################

# Make sure that HOME environment variabile is set in your system before run this script

echo ""
echo ">>> Init script to uninstall passHider <<<"
echo ""

COLOR="\033[0;" # Normal text
COLORBOLD="\033[1;" # Bold text
BLACK="30" # Black FG
RED="31" # Red FG
GREEN="32" # Green FG
BROWN_ORANGE="33" # Brown or Orange FG
BLUE="34" # Blue FG
PURPLE="35" # Purple FG
CYAN="36" # Cyan FG
WHITE="37" # White FG
GRAY="90" # Gray FG
LIGHTRED="91" # Light Red FG
LIGHTGREEN="92" # Light Green FG
YELLOW="93" # Yellow FG
LIGHTBLUE="94" # Light Blue FG
LIGHTPURPLE="95" # Light Purple FG
LIGHTCYAN="96" # Light Cyan FG
BLACK_BG=";40m" # Black BG
RED_BG=";41m" # Red BG
GREEN_BG=";42m" # Green BG
BROWN_ORANGE_BG=";43m" # Brown or Orange BG
BLUE_BG=";44m" # Blue BG
PURPLE_BG=";45m" # Purple BG
CYAN_BG=";46m" # Cyan BG
WHITE_BG=";47m" # White BG
GRAY_BG=";100m" # Gray BG
LIGHTRED_BG=";101m" # Light Red BG
LIGHTGREEN_BG=";102m" # Light Green BG
YELLOW_BG=";10'3m" # Yellow BG
LIGHTBLUE_BG=";104m" # Light Blue BG
LIGHTPURPLE_BG=";105m" # Light Purple BG
LIGHTCYAN_BG=";106m" # Light Cyan BG
LIGHTWHITE_BG=";107m" # Light White BG
NC="\033[0m" # No Color

uninstallConfirm () {
    printf "${COLORBOLD}${PURPLE}${GRAY_BG} Do you want really uninstall passHider ?? [N/y] ${NC}"
    read -p ": " userConfirm
    if [[ "${userConfirm}" == [nN] || "${userConfirm}" == "" ]]; then
        printf "${COLORBOLD}${PURPLE}${GRAY_BG} Exiting doing nothing ${NC}\n\n"
        exit 0 # Exit code Success
    elif [[ "${userConfirm}" == [y] ]]; then
        printf "${COLORBOLD}${PURPLE}${GRAY_BG} Starting uninstall procedure ${NC}\n\n"
    else
        printf "${COLORBOLD}${YELLOW}${GRAY_BG} Wrong choice, admitted values are 'N' 'n' or 'y' ${NC}\n\n"
        uninstallConfirm
    fi
}
workingDirDeleteConfirm () {
    printf "${COLORBOLD}${PURPLE}${GRAY_BG} Do you want really delete ${workingDir} ?? [N/y] ${NC}\n"
    printf "${COLORBOLD}${YELLOW}${GRAY_BG} NOTE that this will erase all passHider data !!! ${NC}"
    read -p ": " userConfirm
    if [[ "${userConfirm}" == [nN] || "${userConfirm}" == "" ]]; then
        printf "${COLORBOLD}${PURPLE}${GRAY_BG} Exiting without removing ${workingDir} ${NC}\n\n"
        exit 0 # Exit code Success
    elif [[ "${userConfirm}" == [y] ]]; then
        printf "${COLORBOLD}${PURPLE}${GRAY_BG} Now I will erase all passHider data ... ${NC}\n\n"
    else
        printf "${COLORBOLD}${YELLOW}${GRAY_BG} Wrong choice, admitted values are 'N' 'n' or 'y' ${NC}\n\n"
        workingDirDeleteConfirm
    fi
}
uninstallConfirm
# Dummy echo used to invoke sudo and cache the password
sudo echo -n # Note also that the use of echo with sudo is useless =)

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Creating useful vars ${NC}\n\n"

outputFileName="passHider" # The executable filename
outputDesktopFileName="passHider.desktop" # The .desktop filename
outputBinPath="/usr/local/bin/" # Where will linked generated executable
outputDirPath="/usr/local/bin/PassHider/" # Where will installed the software
applicationsPath="/usr/share/applications/" # Where are located .desktop file in your system
workingDir="${HOME}/.passHider/" # Where will located the user files and dirs of the software (Should be equal to self.workingDir declared in main.py)
workingBckDir="${HOME}/.passHider/backup/" # Where will located the backuped datafile of the software (The string after ~/.passHider/ should be equal to backupDir declared in conf.ini)
workingOldEncDir="${HOME}/.passHider/oldEncryption/" # Where will located the files and relative key of old encryptions (The string after ~/.passHider/ should be equal to oldEncryptionDir declared in conf.ini)
workingAssetsDir="${HOME}/.passHider/assets/" # Where will stored the configuration file
dataFileName="data.json.enc" # The file that will contain your encrypted credentials (Should be equal to dataFile declared in conf.ini)

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Deleting ${applicationsPath}${outputDesktopFileName} ${NC}\n\n"
sudo rm -rf "${applicationsPath}${outputDesktopFileName}"

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Deleting ${outputDirPath} ${NC}\n\n"
sudo rm -rf "${outputDirPath}"

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Deleting link ${outputBinPath}${outputFileName} ${NC}\n\n"
sudo rm -rf "${outputBinPath}${outputFileName}"

workingDirDeleteConfirm
printf "${COLORBOLD}${PURPLE}${GRAY_BG} Deleting ${workingDir} ${NC}\n\n"
sudo rm -rf "${workingDir}"

exit 0 # Exit code Success