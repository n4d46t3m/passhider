#!/bin/bash

########################################################################
#
# n4d46t3m netcat 2021-2022 copyLeft or MIT
#
#          _  _       _ _  _       
#    _ __ | || |   __| | || |  ___ 
#   | '_ \| || |_ / _` | || |_/ __|
#   | | | |__   _| (_| |__   _\__ \
#   |_| |_|  |_|  \__,_|  |_| |___/                               
#                                     
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
########################################################################

# You should have already installed pyinstaller for example with:
# pip3 install pyinstaller
# Set var pyInstallerPath with the right pyinstaller location in your system
#
# Make sure that HOME environment variabile is set in your system before run this script

echo ""
echo ">>> Init script to generate passHider executable and install it <<<"
echo ""

COLOR="\033[0;" # Normal text
COLORBOLD="\033[1;" # Bold text
BLACK="30" # Black FG
RED="31" # Red FG
GREEN="32" # Green FG
BROWN_ORANGE="33" # Brown or Orange FG
BLUE="34" # Blue FG
PURPLE="35" # Purple FG
CYAN="36" # Cyan FG
WHITE="37" # White FG
GRAY="90" # Gray FG
LIGHTRED="91" # Light Red FG
LIGHTGREEN="92" # Light Green FG
YELLOW="93" # Yellow FG
LIGHTBLUE="94" # Light Blue FG
LIGHTPURPLE="95" # Light Purple FG
LIGHTCYAN="96" # Light Cyan FG
BLACK_BG=";40m" # Black BG
RED_BG=";41m" # Red BG
GREEN_BG=";42m" # Green BG
BROWN_ORANGE_BG=";43m" # Brown or Orange BG
BLUE_BG=";44m" # Blue BG
PURPLE_BG=";45m" # Purple BG
CYAN_BG=";46m" # Cyan BG
WHITE_BG=";47m" # White BG
GRAY_BG=";100m" # Gray BG
LIGHTRED_BG=";101m" # Light Red BG
LIGHTGREEN_BG=";102m" # Light Green BG
YELLOW_BG=";10'3m" # Yellow BG
LIGHTBLUE_BG=";104m" # Light Blue BG
LIGHTPURPLE_BG=";105m" # Light Purple BG
LIGHTCYAN_BG=";106m" # Light Cyan BG
LIGHTWHITE_BG=";107m" # Light White BG
NC="\033[0m" # No Color

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Creating useful vars ${NC}\n\n"

swName="Pass Hider"
swVersion="1.0.10"
swDesc="Visualizza le credenziali volute ma non mostra la password che viene copiata nella clipboard"
outputFileName="passHider" # The executable filename
outputDesktopFileName="passHider.desktop" # The .desktop filename
outputBinPath="/usr/local/bin/" # Where will linked generated executable
outputDirPath="/usr/local/bin/PassHider/" # Where will installed the software
applicationsPath="/usr/share/applications/" # Where are located .desktop file in your system
workingDir="${HOME}/.passHider/" # Where will located the user files and dirs of the software (Should be equal to self.workingDir declared in main.py)
workingBckDir="${HOME}/.passHider/backup/" # Where will located the backuped datafile of the software (The string after ~/.passHider/ should be equal to backupDir declared in conf.ini)
workingOldEncDir="${HOME}/.passHider/oldEncryption/" # Where will located the files and relative key of old encryptions (The string after ~/.passHider/ should be equal to oldEncryptionDir declared in conf.ini)
workingAssetsDir="${HOME}/.passHider/assets/" # Where will stored the configuration file
dataFileName="data.json.enc" # The file that will contain your encrypted credentials (Should be equal to dataFile declared in conf.ini)
# External programs used for installation
pyInstallerPath="${HOME}/.local/bin/pyinstaller" # pyinstaller location
generateDesktopFile () {
    rm -rf "./${outputDesktopFileName}" # Useless for almost all use cases but I don't comment this line
    echo "[Desktop Entry]" >> "./${outputDesktopFileName}"
    echo "Type=Application" >> "./${outputDesktopFileName}"
    echo "Version=${swVersion}" >> "./${outputDesktopFileName}"
    echo "Name=${swName}" >> "./${outputDesktopFileName}"
    echo "Comment=${swDesc}" >> "./${outputDesktopFileName}"
    echo "Path=${HOME}/.passHider/" >> "./${outputDesktopFileName}"
    echo "Exec=${outputDirPath}${outputFileName}" >> "./${outputDesktopFileName}"
    echo "Icon=${outputDirPath}assets/logo.png" >> "./${outputDesktopFileName}"
    echo "Terminal=true" >> "./${outputDesktopFileName}"
    echo "Categories=Utility;" >> "./${outputDesktopFileName}"
}
# Dummy echo used to invoke sudo and cache the password
sudo echo -n # Note also that the use of echo with sudo is useless =)

########################################################################
########################################################################
########################################################################

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Generating executable file ${outputFileName} ${NC}\n\n"
${pyInstallerPath} --onefile --distpath "." -n ${outputFileName} "../main.py"
if test -f "${outputFileName}"; then
    printf "\n${COLORBOLD}${PURPLE}${GRAY_BG} Executable successfully created !! ${NC}\n\n"
else
    printf "\n${COLORBOLD}${WHITE}${RED_BG} Something went wrong creating executable ${NC}\n\n"
    exit 1 # Exit code Error
fi

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Generating directory tree ${NC}\n\n"
[ -d "$workingDir" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${workingDir} found${NC}\n\n" || mkdir "$workingDir"
[ -d "${workingBckDir}" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${workingBckDir} found${NC}\n\n" || mkdir "${workingBckDir}"
[ -d "${workingOldEncDir}" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${workingOldEncDir} found${NC}\n\n" || mkdir "${workingOldEncDir}"
[ -d "${workingAssetsDir}" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${workingAssetsDir} found${NC}\n\n" || mkdir "${workingAssetsDir}"
[ -d "$outputDirPath" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${outputDirPath} found${NC}\n\n" || sudo mkdir -p "$outputDirPath"
[ -d "${outputDirPath}assets/" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${outputDirPath}assets/ found${NC}\n\n" || sudo mkdir "${outputDirPath}assets/" # Where are stored common files such as the software icon
[ -d "${outputDirPath}scripts/" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${outputDirPath}scripts/ found${NC}\n\n" || sudo mkdir "${outputDirPath}scripts/"
[ -d "$applicationsPath" ] && printf "${COLORBOLD}${YELLOW}${GRAY_BG}Directory ${applicationsPath} found${NC}\n\n" || sudo mkdir -p "$applicationsPath"
printf "${COLORBOLD}${PURPLE}${GRAY_BG} Copying icon in ${outputDirPath}assets/ ${NC}\n\n"
sudo cp "../assets/logo.png" "${outputDirPath}/assets/"
sudo chmod 704 "${outputDirPath}/assets/logo.png"
printf "${COLORBOLD}${PURPLE}${GRAY_BG} Copying uninstall script in ${outputDirPath}scripts/ ${NC}\n\n"
sudo cp "../scripts/uninstall.sh" "${outputDirPath}/scripts/"
sudo chmod 705 "${outputDirPath}/scripts/uninstall.sh"
printf "${COLORBOLD}${PURPLE}${GRAY_BG} Copying config file in ${workingAssetsDir} ${NC}\n\n"
if test -f "${workingAssetsDir}conf.ini"; then
    printf "\n${COLORBOLD}${YELLOW}${GRAY_BG} conf.ini already exist in ${workingAssetsDir} !! ${NC}\n\n"
else
    cp "../assets/conf.ini" "${workingAssetsDir}"
fi
touch "${workingDir}${dataFileName}"
chmod -R 700 "${workingDir}"

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Copying ${outputFileName} in ${outputDirPath} ${NC}\n\n"
sudo cp "${outputFileName}" "${outputDirPath}"
sudo ln -s "${outputDirPath}${outputFileName}" "${outputBinPath}${outputFileName}"

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Generating .desktop file ${NC}\n\n"
generateDesktopFile
printf "${COLORBOLD}${PURPLE}${GRAY_BG} Copying .desktop file in ${applicationsPath} ${NC}\n\n"
sudo mv "./${outputDesktopFileName}" "${applicationsPath}"
sudo chown root:root "${applicationsPath}${outputDesktopFileName}"
sudo chmod 644 "${applicationsPath}${outputDesktopFileName}"

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Do you want clean files and useless dir generated during installation ?? [Y/n] ${NC}"
read -p ": " cleanTrashFiles

if [[ "${cleanTrashFiles}" == [yYsS] || "${cleanTrashFiles}" == "" ]]; then
    printf "${COLORBOLD}${PURPLE}${GRAY_BG} Clean useless files ${NC}\n\n"
    rm -rf "./build"
    rm -rf "./${outputFileName}.spec"
else
    printf "${COLORBOLD}${PURPLE}${GRAY_BG} Exiting without clean build files ${NC}\n\n"
fi

printf "${COLORBOLD}${PURPLE}${GRAY_BG} Pass Hider is installed successfully, to uninstall it run ${outputDirPath}scripts/uninstall.sh with root privileges ${NC}\n\n"
printf "${COLORBOLD}${PURPLE}${GRAY_BG} Script terminated, see you soon... ${NC}\n\n"

exit 0 # Exit code Success
