#!/usr/bin/python3

########################################################################
#
# Copyright (c) 2021-2022 n4d46t3m
#
#          _  _       _ _  _       
#    _ __ | || |   __| | || |  ___ 
#   | '_ \| || |_ / _` | || |_/ __|
#   | | | |__   _| (_| |__   _\__ \
#   |_| |_|  |_|  \__,_|  |_| |___/                               
#  
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
########################################################################

from cryptography.fernet import Fernet
import json # Testing DEBUG

class EncryptDecrypt:

    def __init__(self,originalFileName='',keyFileName='',encryptedFileName='',verbose=False,debug=False):
        self.swName='ecryptDecrypt' # Software name
        self.swVers='1.0.1' # Software version
        self.originalFileName=originalFileName # Some file containing the text that must you want encrypt
        self.keyFileName=keyFileName # Some file that contain the key used for encrypt/decrypt data
        self.encryptedFileName=encryptedFileName # Some file that contain encrypted text
        self.fernetInitialized = None # Will contain Fernet object initialized with the key found in self.keyFileName
        self.debug=debug#Not yet implemented very well
        self.verbose=verbose#Not yet implemented very well
        
    def getKeyFileName(self):
        '''
        getKeyFileName 1.0
        Return a string that contain the value of self.keyFileName
        '''
        return self.keyFileName
        
    def write_key(self, openMode='wb', fileToWrite=''):
        '''
        write_key 1.1
        Generates a key and save it into a file
        '''
        if fileToWrite == '':
            fileToWrite = self.keyFileName
        key = Fernet.generate_key()
        with open(fileToWrite, openMode) as key_file:
            key_file.write(key)
            
    def load_key(self, openMode='rb', keyToLoad=''):
        '''
        load_key 1.1
        Loads the key from the current directory
        '''
        if keyToLoad == '':
            keyToLoad = self.keyFileName
        return open(keyToLoad, openMode).read()
        
    def initFernet(self,key=''):
        '''
        initFernet 1.0
        Initialize Fernet with given key
        '''
        self.fernetInitialized = Fernet(key)# initialize the Fernet class
        
    def encryptString(self,clearString=''):
        '''
        encryptString 1.0
        Encrypt given string with given key
        '''
        return self.fernetInitialized.encrypt(clearString)# encrypt the message
        
    def decryptString(self,encryptedString=''):
        '''
        decryptString 1.0
        Decrypt given encrypted string
        '''
        return self.fernetInitialized.decrypt(encryptedString)#Decrypting the encrypted string
        
    def loadTextFile(self,fileToLoad=''):
        '''
        loadTextFile 1.0
        Load given text file in a string
        '''
        with open(fileToLoad) as inputFile:
            return inputFile.read().encode()#We need to encode strings, to convert them to bytes to be suitable for encryption, encode() method encodes that string using utf-8 codec.
        
    def writeEncryptedStringToFile(self,encryptedString='',openMode='wb',fileToWrite=''):
        '''
        writeEncryptedStringToFile 1.1
        Write an encrypted string to a file
        '''
        if fileToWrite == '':
            fileToWrite = self.encryptedFileName
        textFile = open(fileToWrite,openMode)
        textFile.write(encryptedString)
        textFile.close()
        
########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    
    encDec = EncryptDecrypt('data.json','key.key','data.json.enc',True,True)
    # This is for given strings
    #secretMessage = 'Some secret message !!!'.encode()#We need to encode strings, to convert them to bytes to be suitable for encryption, encode() method encodes that string using utf-8 codec.
    # This is for string that you generate from file
    secretMessage = encDec.loadTextFile(encDec.originalFileName)
    encDec.write_key()# generate and write a new key
    key = encDec.load_key()# load the previously generated key
    encDec.initFernet(key)
    encrypted = encDec.encryptString(secretMessage)
    encDec.writeEncryptedStringToFile(encrypted)
    if(encDec.verbose):
        print(encrypted)# This is the encrypted string
    decrypted_encrypted = encDec.decryptString(encrypted)#Decrypting the encrypted string
    if(encDec.verbose):
        print(decrypted_encrypted)#The original secret string
    if(encDec.verbose or encDec.debug):
        print(json.loads(decrypted_encrypted))#DEBUG
    
